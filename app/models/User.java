package models;

import javax.persistence.*;

import play.db.jpa.Model;

@Entity
public class User extends Model
{
  public Boolean usCitizen;
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  public Boolean conditionsAgree;

  public User(Boolean isUSCitizen, String firstName, String lastName, String email, String password, Boolean conditionsAgree)
  {
	this.usCitizen = isUSCitizen;
    this.firstName = firstName;
    this.lastName  = lastName;
    this.email     = email;
    this.password  = password;
    this.conditionsAgree = conditionsAgree;
  }
  
  public static User findByEmail(String email)
  {
	return find("email", email).first();
  }
  
  public boolean checkPassword(String password)
  {
	return this.password.equals(password);
  }
}
