package models;

import javax.persistence.*;
import play.db.jpa.*;
import java.util.Date;

@Entity
public class Donation extends Model 
{
  public long amountDonated;
  public String methodDonated;
  public Date dateDonated;
  
  @ManyToOne
  public User madeBy;
  
  public Donation(long amountDonated, String methodDonated, User madeBy)
  {
	this.amountDonated = amountDonated;
    this.methodDonated = methodDonated;
    this.madeBy = madeBy;
    this.dateDonated = new Date();
  }
}
