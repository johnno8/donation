package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{

  public static void logout()
  {
	session.clear();
	Welcome.index();
  }

  public static void signup()
  {
	render();
  }

  public static void login()
  {
	render();
  }

  public static void register(Boolean isUSCitizen, String firstName, String lastName, String email, String password, Boolean conditionsAgree)
  {
    Logger.info(isUSCitizen + " " + firstName + " " + lastName + " " + email + " " + password + " " + conditionsAgree);

    User user = new User (isUSCitizen, firstName, lastName, email, password, conditionsAgree);
    user.save();

    login();
  }
  
  public static void authenticate(String email, String password)
  { 
	Logger.info("Attempting to authenticate with " + email + " : " + password);
	User user = User.findByEmail(email);
	if((user != null) && (user.checkPassword(password) == true))
	{
	  Logger.info("Authentication successful");
	  session.put("logged_in_userid", user.id);
	  Welcome.index();
	}
	else
	{
	  Logger.info("Authentication failed");
	  login();
    }
  }
  
  
}
