package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Donate extends Controller 
{ 
  public static void index()
  {
	String userId = session.get("logged_in_userid");
	User user = User.findById(Long.parseLong(userId));
	float progress = calculateProgress();
    render(user, progress);
  }
	
  public static void donate(long amountDonated, String methodDonated)
  {
	
	{
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    Donation donation = new Donation(amountDonated, methodDonated, user);
    donation.save();
      
    index();
	}
  }
  
  public static void report()
  {
	List<Donation> donations = Donation.findAll();
	render(donations);
  }
  
  public static float calculateProgress()
  {
	long totalToDate = 0;
	List<Donation> donations = Donation.findAll();
	for(Donation donation : donations)
	{
	  totalToDate += donation.amountDonated;
	}
	return (float) (totalToDate * 100) / 20000;
  }
  
  
}
